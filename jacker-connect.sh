#! /bin/env bash
set -x
# Wrapper script for jacker for as long as we dont

## get the snd virmidi card id
sudo modprobe snd-virmidi
virmidi_id=$(echo $(cat /proc/asound/cards | grep VirMIDI) | cut -d\  -f 1)

./midi_jacker.py connect --vid ${virmidi_id}

# TODO: implement in python and integrate in main code
