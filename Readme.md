Jacker. A tool for Jack Audio Connection Kit Midi management.

## TODO:

* Cleanup and sort stuff
* add more features:
    * load connections from given filename
    * find virmidi id in system config automatically for replacement
    * split connections function in save / show or just add save flag so 
      its always shown but only optionally saved
    * save current connections to json a using given filename when available, 
      but keep the default saving to datestamped file
    * disconnect all ports
    * add midi monitoring properly
    * gui to set connections matrix style and save/load/edit presets
    * cli shell with autocompletion for command execution