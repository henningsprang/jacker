#! /bin/env python3
import json
import datetime

import click
import jack
from click import echo
from jack import JackError
import os

"""
Jacker. A tool for Jack Audio Connection Kit Midi management.
"""


def get_connections(connections_filename='connections.json'):
    # load from file

    with open(connections_filename) as connections_file:
        connections = json.load(connections_file)

    return connections


client_name = 'Jacker MIDI Tool'


@click.group()
def cli():
    pass


@cli.command()
@click.option('-f', 'connections_filename', help='Name of connections file.', default=None)
def connections(connections_filename):
    # read all midi connections and print them sorted by source and save file
    client = jack.Client(client_name)

    ports = client.get_ports(is_midi=True, is_output=True)

    save_connections = dict()
    for port in ports:
        connections = client.get_all_connections(port)
        if not connections:
            continue

        save_port_name = port.aliases[1]
        save_connections[save_port_name] = list()
        echo(F' {save_port_name}')
        for connection in connections:
            save_connection_port_name = connection.aliases[1]
            save_connections[save_port_name].append(save_connection_port_name)
            echo(f'  -> {save_connection_port_name}')
        echo()

    if connections_filename is None:
        connections_filename = f'connections_{datetime.datetime.now()}.json'

    with open(connections_filename, 'w') as json_file:
        json.dump(save_connections, json_file, indent=2)

    last_connections_link = 'connections_last.json'

    if os.path.exists(last_connections_link):
        os.remove(last_connections_link)

    os.symlink(connections_filename, last_connections_link)


@cli.command()
def ports():

    echo('Available Ports:')

    client = jack.Client(client_name)

    ports = client.get_ports(is_midi=True)

    if ports is None or len(ports) == 0:
        echo('No ports found.')
        echo(client.status)
    else:
        echo(ports)

    for port in ports:
        echo(F' {port.name}')
        for alias in port.aliases:
            echo(F'  {alias}')
        if port.is_input:
            echo('  IN')
        else:
            echo('  OUT')
        echo(F' {port.uuid}')
        echo()


def replace_virmidi_id(port_name, virmidi_id):
    """
    Replace the virmidi id in a stored port name.
    Needed because the id's change on system reboots when
    the devices are not added to the system (e.g.  powered on)
    in the same order.
    This code depends on the naming scheme

    Virtual-Raw-MIDI-9-2:midi/playback_1

    where 9 is the id. It can also have more figures, like 10.

    :param port_name:
    :param virmidi_id:
    :return:
    """
    if not port_name.startswith('Virtual-Raw-MIDI-'):
        # nothing to do
        return port_name

    splitted = port_name.split('-')
    before_id = '-'.join(splitted[0:3])
    after_id = '-'.join(splitted[4:])

    new_port_name = f'{before_id}-{virmidi_id}-{after_id}'

    return new_port_name


@cli.command()
@click.option('--vid', 'virmidi_card_id', help='VirMIDI Soundcard ID')
@click.option('-f', 'connections_filename', help='Name of connections file.', default='connections.json')
def connect(virmidi_card_id, connections_filename):
    client = jack.Client(client_name)

    for source_name, destination_names in get_connections(connections_filename).items():

        source_name = replace_virmidi_id(source_name, virmidi_card_id)
        echo(source_name)

        try:
            source_port = client.get_port_by_name(source_name)
        except JackError as e:
            echo(e)
            echo('source port {} not found - skipping'.format(source_name))
            continue

        for destination_name in destination_names:
            destination_name = replace_virmidi_id(
                destination_name, virmidi_card_id)

            try:
                dest_port = client.get_port_by_name(destination_name)
            except JackError as e:
                echo(e)
                echo('dest port {} not found - skipping'.format(destination_name))
                continue

            echo('Connecting {} to {}'.format(source_port.aliases[0], dest_port.aliases[0]))

            try:
                client.connect(source_port, dest_port)
            except JackError as e:
                echo(e)

if __name__ == '__main__':
    cli()
